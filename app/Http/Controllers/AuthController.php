<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }

    public function welcome(){
        return view('welcome');
    }

    public function kirim(Request $request){
        $namadpn = $request['namadepan'];
        $namablkg = $request['namablkng'];
        //$alamat = $request['address'];
        return view('welcome', compact('namadpn','namablkg'));
    }
}
